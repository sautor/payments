<?php

namespace Sautor\Payments\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Inscricao;
use Sautor\Payments\Models\Payment;

class PaymentsController extends Controller
{
    const PER_PAGE = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Grupo $grupo)
    {
        // TODO: Improve this policy
        $this->authorize('update', $grupo);
        $payments = Payment::where('grupo_id', $grupo->id)->where(function ($query) {
            $query->where('school_year', \Sautor\anoLetivo())
                ->orWhere('school_year', null);
        })->orderByDesc('paid_at')->paginate(self::PER_PAGE);

        return view('payments::index', compact('grupo', 'payments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->validate($request, [
            'amount' => 'required|numeric',
            'description' => 'required',
            'paid_at' => 'required|date',
            'pessoa_id' => 'required|exists:pessoas,id',
        ]);

        $incricao = Inscricao::active()->where('pessoa_id', $request->pessoa_id)->where('grupo_id', $grupo->id)->firstOrFail();
        Payment::create([
            'amount' => $request->amount,
            'description' => $request->description,
            'paid_at' => $request->paid_at,
            'grupo_id' => $incricao->grupo_id,
            'pessoa_id' => $incricao->pessoa_id,
            'school_year' => $incricao->ano_letivo,
        ]);

        Notification::make()
            ->title('Pagamento criado com sucesso.')
            ->success()
            ->send();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Grupo $grupo, Payment $payment)
    {
        $this->authorize('update', $grupo);
        $payment->delete();
        Notification::make()
            ->title('Pagamento eliminado com sucesso.')
            ->success()
            ->send();

        return back();
    }
}
