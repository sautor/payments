<?php

namespace Sautor\Payments;

use Illuminate\Support\ServiceProvider;
use Sautor\Core\Services\Addons\Addon;

class PaymentsServiceProvider extends ServiceProvider
{
    private Addon $addon;

    /**
     * PaymentsServiceProvider constructor.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->addon = Addon::create('payments', 'Pagamentos', 'money-bill-wave', 'Extensão para controlo de pagamentos num grupo.')
            ->setEntryRouteForGroup('payments.index')
            ->withAssets();
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'sautor');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'payments');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $addonService = $this->app->make('AddonService');
        $addonService->register($this->addon);
    }
}
