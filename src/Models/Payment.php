<?php

namespace Sautor\Payments\Models;

use Illuminate\Database\Eloquent\Model;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Pessoa;

class Payment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pay_payments';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'paid_at' => 'datetime',
    ];

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }
}
