<?php

use Sautor\Payments\Controllers\PaymentsController;

Sautor\groupRoute(function () {
    Route::middleware(['auth', 'addon:payments'])->group(function () {
        Route::get('pagamentos', [PaymentsController::class, 'index'])->name('payments.index');
        Route::post('pagamentos', [PaymentsController::class, 'store'])->name('payments.store');
        Route::delete('pagamentos/{payment}', [PaymentsController::class, 'destroy'])->name('payments.destroy');
    });
});
