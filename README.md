# Sautor Payments

Extensão de Pagamentos para o sistema Sautor.

## Instalação

Instalar via Composer:
````sh
composer require sautor/payments
````

Para correr a migração:
````sh
php artisan migrate
````
