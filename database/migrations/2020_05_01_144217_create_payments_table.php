<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_payments', function (Blueprint $table) {
            $table->id();
            $table->integer('grupo_id')->unsigned();
            $table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('cascade');
            $table->integer('pessoa_id')->unsigned();
            $table->foreign('pessoa_id')->references('id')->on('pessoas')->onDelete('cascade');
            $table->integer('school_year')->nullable();
            $table->string('description');
            $table->double('amount', 8, 2);
            $table->date('paid_at');
            $table->timestamps();
        });

        if (Schema::hasTable('pagamentos')) {
            $old_payments = DB::table('pagamentos')->get();
            $updated_payments = $old_payments->map(function ($payment) {
                return [
                    'id' => $payment->id,
                    'grupo_id' => $payment->grupo_id,
                    'pessoa_id' => $payment->pessoa_id,
                    'school_year' => $payment->ano_letivo,
                    'description' => $payment->descricao,
                    'amount' => $payment->valor,
                    'paid_at' => $payment->data,
                    'created_at' => $payment->created_at,
                    'updated_at' => $payment->updated_at,
                ];
            });
            DB::table('pay_payments')->insert($updated_payments->all());

            Schema::dropIfExists('pagamentos');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_payments');
    }
}
