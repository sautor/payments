@extends('layouts.group-admin')

@section('group-content')
        <div class="group-page__header">
            <h1 class="group-page__header__title">
                Pagamentos
            </h1>

            <div class="group-page__header__actions">
                <button type="button" class="button button--responsive" @click.prevent="openModal('addPayment')">
                    <span class="far fa-plus"></span>
                    Registar pagamento
                </button>
            </div>
        </div>

        @if($payments->isNotEmpty())
            <table class="s-pay-w-full s-pay-border-collapse s-pay-mb-4 s-pay-bg-white s-pay-rounded-lg s-pay-shadow s-pay-overflow-hidden">
                <thead class="s-pay-bg-primary-100 s-pay-text-primary-700">
                <tr>
                    <th class="s-pay-uppercase s-pay-font-bold s-pay-text-sm s-pay-py-3 s-pay-px-2 s-pay-text-left">
                        Data
                    </th>
                    <th class="s-pay-uppercase s-pay-font-bold s-pay-text-sm s-pay-py-3 s-pay-px-2 s-pay-text-left">
                        Pessoa
                    </th>
                    <th class="s-pay-uppercase s-pay-font-bold s-pay-text-sm s-pay-py-3 s-pay-px-2 s-pay-text-left">
                        Descrição
                    </th>
                    <th class="s-pay-uppercase s-pay-font-bold s-pay-text-sm s-pay-py-3 s-pay-px-2 s-pay-text-left">
                        Valor
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td class="s-pay-border-t s-pay-py-3 s-pay-px-2">
                            {{ $payment->paid_at->isoFormat('LL') }}
                        </td>
                        <td class="s-pay-border-t s-pay-py-3 s-pay-px-2">
                            <div class="s-pay-flex s-pay-gap-2 s-pay-items-center">
                                <img src="{{ $payment->pessoa->foto }}"
                                     class="s-pay-w-8 s-pay-h-8 s-pay-rounded-full -s-pay-my-2" />
                                {{ $payment->pessoa->nome_exibicao }}
                            </div>
                        </td >
                        <td class="s-pay-border-t s-pay-py-3 s-pay-px-2">
                            {{ $payment->description }}
                        </td>
                        <td class="s-pay-border-t s-pay-py-3 s-pay-px-2 s-pay-text-right s-pay-tabular-nums">
                            &euro;&nbsp;{{ number_format($payment->amount, 2, ',', '') }}
                        </td>
                        <td class="s-pay-border-t s-pay-py-3 s-pay-px-2 s-pay-text-right">
                            <button class="button button--icon button--danger-outline button--small" type="button"
                                    @click.prevent="openModal('removePayment{{ $payment->id }}')">
                                <span class="far fa-trash fa-fw"></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $payments->links() }}

        @else

            <section class="sautor-list--empty">
                Não existem pagamentos neste grupo.
            </section>

        @endif

    <modal id="addPayment">
        @php($scope = 'adicionar-pagamento')
        <form method="POST" action="{{ $grupo->route('payments.store') }}"
              data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
            @csrf
            <div class="modal__body">
                <h3 class="modal__title">Registar pagamento</h3>

                <x-forms.input name="description" label="Descrição" required rules="required" :scope="$scope" />
                <div class="grid grid-cols-2 gap-2">
                    <x-forms.input name="amount" label="Valor" required rules="required|decimal:2"
                                   before='<span class="far fa-fw fa-euro-sign"></span>' type="number"
                                   :options="['min' => '0', 'step' => '0.01']" :scope="$scope" />
                    <x-forms.input name="paid_at" label="Data" required rules="required|date_format:yyyy-MM-dd" type="date" :scope="$scope" />
                </div>

                <div class="field" :class="{ 'field--invalid': errors.has('adicionar-pagamento.pessoa_id') }">
                    <label class="label">Pessoa</label>
                    <div class="control">
                        <pessoa-picker name="pessoa_id" grupo-id="{{ $grupo->id }}" with-input v-validate="'required'" class="s-pay-flex-1" />
                    </div>
                    <span class="field__help field__help--invalid" v-if="errors.has('adicionar-pagamento.pessoa_id')" v-html="errors.first('adicionar-pagamento.pessoa_id')"></span>
                </div>

            </div>
            <div class="modal__footer">
                <button class="button button--primary">
                    Registar
                </button>
                <button class="button" type="button" @click.prevent="closeModal('addPayment')">
                    Cancelar
                </button>
            </div>
        </form>
    </modal>


    @foreach($payments as $payment)
        <modal id="removePayment{{ $payment->id }}">
            <div class="modal__body modal__confirmation">
                <div class="modal__confirmation__icon">
                    <span class="fas fa-exclamation"></span>
                </div>
                <div class="modal__confirmation__content">
                    <h3 class="modal__confirmation__title">Remover pagamento</h3>
                    <p class="modal__confirmation__text">
                        Tem a certeza que quer remover o pagamento de <strong>{{ $payment->pessoa->nome_exibicao }}</strong> a
                        <strong>{{ $payment->paid_at->isoFormat('LL') }}</strong>?
                    </p>
                </div>
            </div>
            <form class="modal__footer" method="POST" action="{{ $grupo->route('payments.destroy', $payment) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="button button--danger">Eliminar</button>
                <button type="button" class="button" @click.prevent="closeModal('removePayment{{ $payment->id }}')">Cancelar</button>
            </form>
        </modal>
    @endforeach
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/payments/styles.css') }}">
@endpush
